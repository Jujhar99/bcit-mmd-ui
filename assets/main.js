// Calculate scroll position on root-image box move
$("#wrapper").scroll(function () {
    var position = document.getElementById('wrapper').scrollLeft;
    console.log(position);

    // Hide all
    $(".section-info-message-class").hide();

    // Display message 1
    if (position < 137) {
        $("#section-info-message-1").show();
        $("#section-info-line1").show();
    }

    // Display message 2
    else if (position > 138 && position < 287) {
        $("#section-info-message-2").show();
        $("#section-info-line2").show();
    }

    // Display message 3
    else if (position > 287 && position < 345) {
        $("#section-info-message-3").show();
        $("#section-info-line2a").show();
    }

    // Display message 4
    else if (position > 345 && position < 437) {
        $("#section-info-message-4").show();
        $("#section-info-line2a").show();
    }

    // Display message 5
    else if (position > 437 && position < 537) {
        $("#section-info-message-5").show();
        $("#section-info-line2c").show();
    }

    // Display message 6
    else if (position > 537) {
        $("#section-info-message-6").show();
        $("#section-info-line3").show();
    }

});